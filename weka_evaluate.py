import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix


# weka_evaluate脚本目的：使用scikit-learn评估weka的输出结果
class WekaEval:
    def __init__(self,OUT_PATH,ANS_PATH, task):
        # 指定做哪个任务
        self.task = task
        # 读取weka的输出文件，答案文件
        self.output = pd.read_csv(OUT_PATH)
        self.answer = pd.read_csv(ANS_PATH, header=None)  # 答案文件无列名，所以设置header为None
        self.mapping = {'a': {'OFF': 1, 'NOT': 0}, 'b': {'TIN': 1, 'UNT': 0}, 'c': {'IND': 0, 'GRP': 1, 'OTH': 2}}

    # 将nominal标签转为数字以兼容sklearn的评估函数
    def label2num(self):
        # 映射(OFF|NOT)->(1|0), (TIN|UNT)->(1|0), (IND|GRP|OTH)->(0|1|2)
        self.output['predicted'] = self.output['predicted'].map(lambda x: self.mapping[self.task][x[2:]])
        self.answer[1] = self.answer[1].map(lambda x: self.mapping[self.task][x])

    # 查看样本
    def sample(self):
        print(self.output.head())
        print(self.answer.head())

    # 评估结果
    def eval(self):
        print("Accuracy Score -> ", accuracy_score(self.answer[1], self.output['predicted']))
        print("precision Score -> ", precision_score(self.answer[1], self.output['predicted'], average='macro'))
        print("recall Split -> ", recall_score(self.answer[1], self.output['predicted'], average=None))
        print("recall Macro -> ", recall_score(self.answer[1], self.output['predicted'], average='macro'))
        print("F1-Split -> ", f1_score(self.answer[1], self.output['predicted'], average=None))
        print("F1-Macro -> ", f1_score(self.answer[1], self.output['predicted'], average='macro'))
        print("confusion matrix->\n ", confusion_matrix(self.answer[1], self.output['predicted']))


if __name__ == '__main__':
    # 设定weka输出文件和答案文件的路径
    task = 'c'
    OUT_PATH = 'weka-output/output_%s.csv' % task
    ANS_PATH = 'OLID/labels-level%s.csv' % task
    # 评估哪一个subtask? 输入a/b/c
    weka = WekaEval(OUT_PATH, ANS_PATH, task)
    print(weka.mapping)
    weka.label2num()
    weka.eval()
