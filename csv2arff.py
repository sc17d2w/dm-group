import pandas as pd


# 根据csv的字段构建ARFF的attribute
# 参数df为pandas的DataFrame
# 参数default_type为当列是对象时，该对象的默认类型
def get_attr(df, default_type='string'):
    # pandas的数字类型
    numeric = ['float64', 'int64']
    attributes = []

    # 为每个字段构建一个attribute字符串
    for field in df.columns.values.tolist():
        if field in labels:  # 对标签类特殊处理
            left = '@attribute %s {' % field
            for item in df[field].unique():
                left += '%s,' % item
            left = left[:-1] + '}'
            attributes.append(left)
            continue
        # print("{}:{}".format(field, csvf[field].dtype))

        if df[field].dtype in numeric:
            attributes.append("@attribute %s numeric" % field)
        else:
            attributes.append("@attribute %s %s" % (field, default_type))
    return attributes

def convert(paths, labels):
    for path in paths:
        try:
            csvf = pd.read_csv(path)
            encode = 'utf-8'
        except UnicodeDecodeError:
            csvf = pd.read_csv(path, encoding='ISO-8859-1')
            encode = 'ISO-8859-1'

        try:
            # 确保没有会导致arff读取出错的符号
            csvf['tweet'] = csvf['tweet'].str.replace('\"', '')
            csvf['tweet'] = csvf['tweet'].str.replace('\'', '')
            csvf['tweet'] = csvf['tweet'].str.replace(',', '')

            # 给string列加单引号
            csvf['tweet'] = csvf['tweet'].map(lambda x: '\'' + x + '\'')
            csvf['tweet'] = csvf['tweet'].str.lower()
        except:
            pass
        csvf.to_csv('temp.csv', index=None)

        # 创建ARFF独有元素
        attr = get_attr(csvf)
        relation = '@relation %s' % path[:-4]
        data_token = '@data'



        # # 输出文件
        with open('temp.csv', 'r+', encoding=encode) as f:
            old = f.readlines()[1:]

        with open(path[:-4] + '.arff', 'w', encoding=encode) as f:
            f.write(relation + '\n')
            f.writelines('\n'.join(attr))
            f.write('\n' + data_token + '\n\n')
            f.writelines(old)


if __name__ == '__main__':
    # 读取文件
    paths = ["featured_train_b.csv"]

    # 声明标签类，即分类目标
    labels = ['subtask_b']
    convert(paths, labels)

