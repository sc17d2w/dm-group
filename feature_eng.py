import pandas as pd
import collections
import string
import seaborn as sns
import matplotlib.pyplot as plt

from nltk import pos_tag
from nltk.corpus import stopwords


# 获取文本中各词性单词的数量
def tag_part_of_speech(text):
    text_splited = text.split(' ')
    text_splited = [''.join(c for c in s if c not in string.punctuation) for s in text_splited]
    text_splited = [s for s in text_splited if s]
    pos_list = pos_tag(text_splited)
    noun_count = len([w for w in pos_list if w[1] in ('NN','NNP','NNPS','NNS')])
    adjective_count = len([w for w in pos_list if w[1] in ('JJ','JJR','JJS')])
    verb_count = len([w for w in pos_list if w[1] in ('VB','VBD','VBG','VBN','VBP','VBZ')])
    pronoun_count = len([w for w in pos_list if w[1] in 'PRP'])
    return[noun_count, adjective_count, verb_count, pronoun_count]

# 计算重复的单词数
repeated_threshold = 15
def count_repeated(text):
    text_splitted = text.split()
    word_counts = collections.Counter(text_splitted)
    return sum(count for word, count in sorted(word_counts.items()) if count > repeated_threshold)

def field_calc(train):
    train['a_OFF'] = train['subtask_a'].apply(lambda x: 1 if x == 'OFF' else 0)

    train['b_TIN'] = train['subtask_b'].apply(lambda x: 1 if x == 'TIN' else 0)

    train['c_IND'] = train['subtask_c'].apply(lambda x: 1 if x == 'IND' else 0)
    train['c_GRP'] = train['subtask_c'].apply(lambda x: 1 if x == 'GRP' else 0)
    train['c_OTH'] = train['subtask_c'].apply(lambda x: 1 if x == 'OTH' else 0)

    # train['tweet'] = train['tweet'].str.replace('@USER', '')
    # train['tweet'] = train['tweet'].str.replace('URL', '')

    # 初始化字段变量
    COMMENT = 'tweet'

    # 构建新的特征字段列
    # train['total_len'] = train[COMMENT].apply(len)
    train['words'] = train[COMMENT].apply(lambda comment: len(comment.split()))
    # train['words/len'] = train['words'] / train['total_len']
    train['CAPs'] = train[COMMENT].apply(lambda comment: sum(1 for c in comment if c.isupper()))
    # train['CAPs/len'] = train['CAPs'] / train['total_len']
    train['CAPs/words'] = train['CAPs'] / train['words']
    eng_stopwords = set(stopwords.words("english"))
    train['stopwords'] = train[COMMENT].apply(lambda comment: sum(comment.count(w) for w in eng_stopwords))
    # train['stopwords/len'] = train['stopwords'] / train['total_len']
    train['stopwords/words'] = train['stopwords'] / train['words']
    train['PUNC'] = train[COMMENT].apply(
        lambda comment: sum(comment.count(w) for w in string.punctuation))
    # train['PUNC/len'] = train['PUNC'] / train['total_len']
    train['PUNC/words'] = train['PUNC'] / train['words']

    train['symbols'] = train[COMMENT].apply(
        lambda comment: sum(comment.count(w) for w in '*&#$%“”¨«»®´·º½¾¿¡§£₤‘’'))
    # train['symbols/len'] = train['symbols'] / train['total_len']
    train['symbols/words'] = train['symbols'] / train['words']
    train['nouns'], train['adj'], train['verbs'], train['PRP'] = zip(*train[COMMENT].apply(
        lambda comment: tag_part_of_speech(comment)))
    # train['nouns/len'] = train['nouns'] / train['total_len']
    # train['adj/len'] = train['adj'] / train['total_len']
    # train['verbs/len'] = train['verbs'] / train['total_len']
    # train['PRP/len'] = train['PRP'] / train['total_len']
    train['nouns/words'] = train['nouns'] / train['words']
    train['adj/words'] = train['adj'] / train['words']
    train['verbs/words'] = train['verbs'] / train['words']
    train['PRP/words'] = train['PRP'] / train['words']

    # 特定于本数据集的特征
    # train['@USER'] = train[COMMENT].apply(lambda comment: comment.count('@USER'))
    train['@USER'] = train[COMMENT].apply(lambda comment: 1 if comment.find('@USER') > -1 else 0)
    train['URL'] = train[COMMENT].apply(lambda comment: 1 if comment.find('URL') > -1 else 0)
    return train

if __name__ =='__main__':
    # 用pandas读取tsv文件
    data = pd.read_csv('OLID/olid-training-v1.0.tsv', sep='\t')

    LABELS_a = ['a_OFF']
    LABELS_b = ['b_TIN']
    LABELS_c = ['c_IND', 'c_GRP', 'c_OTH']
    train_a = field_calc(data.copy())

    # 单独分析task B
    train_b = field_calc(data[data['subtask_b'].isin(['TIN', 'UNT'])].copy())
    # print(train['subtask_a'].value_counts())

    # 单独分析task C
    train_c = field_calc(data[data['subtask_c'].isin(['IND', 'GRP','OTH'])].copy())


    # 为方便处理，用数字代替off和not
    # train['subtask_a'].replace('OFF', 1, inplace=True)
    # train['subtask_a'].replace('NOT', 0, inplace=True)
    #
    # train['subtask_b'].replace('TIN', 1, inplace=True)
    # train['subtask_b'].replace('UNT', 0, inplace=True)

    # train['subtask_c'].replace('IND', 1, inplace=True)
    # train['subtask_c'].replace('GRP', 0, inplace=True)
    # train['subtask_c'].replace('OTH', 0, inplace=True)



    # 对OffenseEval无用的特征
    # comments features that not applicable to this data set
    # train['mentions'] = train[COMMENT].apply(
    #     lambda comment: comment.count("User:"))
    # train['mentions/len'] = train['mentions'] / train['total_len']
    # train['mentions/words'] = train['mentions'] / train['words']

    # train['paragraphs'] = train[COMMENT].apply(lambda comment: comment.count('\n'))
    # train['paragraphs/len'] = train['paragraphs'] / train['total_len']
    # train['paragraphs/words'] = train['paragraphs'] / train['words']

    # train['smilies'] = train[COMMENT].apply(
    #     lambda comment: sum(comment.count(w) for w in (':-)', ':)', ';-)', ';)')))
    # train['smilies/len'] = train['smilies'] / train['total_len']
    # train['smilies/words'] = train['smilies'] / train['words']
    # train['exclamation_marks'] = train[COMMENT].apply(lambda comment: comment.count('!'))
    # train['exclamation_marks/len'] = train['exclamation_marks'] / train['total_len']
    # train['exclamation_marks/words'] = train['exclamation_marks'] / train['words']
    # train['question_marks'] = train[COMMENT].apply(lambda comment: comment.count('?'))
    # train['question_marks/len'] = train['question_marks'] / train['total_len']
    # train['question_marks/words'] = train['question_marks'] / train['words']

    # train['unique'] = train[COMMENT].apply(
    #     lambda comment: len(set(w for w in comment.split())))
    # train['unique/len'] = train['unique'] / train['total_len']
    # train['unique/words'] = train['unique'] / train['words']
    # train['repeat'] = train[COMMENT].apply(lambda comment: count_repeated(comment))
    # train['repeat/len'] = train['repeat'] / train['total_len']
    # train['repeat/words'] = train['repeat'] / train['words']



    # features = ('total_len',
    #             'words', 'words/len',
    #             'CAPs', 'CAPs/len', 'CAPs/words',
    #             'stopwords', 'stopwords/len', 'stopwords/words',
    #             'PUNC', 'PUNC/len', 'PUNC/words',
    #             'symbols', 'symbols/len', 'symbols/words',
    #             'nouns', 'nouns/words', 'nouns/len',
    #             'adj', 'adj/words', 'adj/len',
    #             'verbs', 'verbs/words', 'verbs/len',
    #             'PRP', 'PRP/words', 'PRP/len',
    #             '@USER','URL'
    #             )

    features = ('words',
                'CAPs', 'CAPs/words',
                'stopwords', 'stopwords/words',
                'PUNC',  'PUNC/words',
                'symbols', 'symbols/words',
                'nouns', 'nouns/words',
                'adj', 'adj/words',
                'verbs', 'verbs/words',
                'PRP', 'PRP/words',
                '@USER','URL'
                )

    # 计算相关性
    # train['none'] = 1 - train[LABELS].max(axis=1)
    # columns = LABELS + ['none']
    columns = LABELS_a
    print(columns)
    rows_a = [{c: train_a[f].corr(train_a[c]) for c in columns} for f in features]
    columns = LABELS_b
    print(columns)
    rows_b = [{c: train_b[f].corr(train_b[c]) for c in columns} for f in features]
    columns = LABELS_c
    print(columns)
    rows_c = [{c: train_c[f].corr(train_c[c]) for c in columns} for f in features]


    for i in range(len(rows_a)):
        rows_a[i].update(rows_b[i])
        rows_a[i].update(rows_c[i])
    rows = rows_a


    train_correlations = pd.DataFrame(rows, index=features)
    # print(train_correlations.loc[:, 'subtask_b'].apply(lambda x: x if x is not None else 0))
    # positive_corr_list = [feature for feature in features if train_correlations.loc[feature, 'a_OFF'] > 0.08]
    # negative_corr_list = [feature for feature in features if train_correlations.loc[feature, 'a_OFF'] < -0.08]
    # high_corr_list = positive_corr_list+negative_corr_list
    # print(high_corr_list)

    text = "ww"


    # 展示heatmap
    fig, ax = plt.subplots(figsize=(8, 8))
    ax = sns.heatmap(train_correlations, annot=True, vmin=-0.25, vmax=0.25, center=0.0, ax=ax)
    plt.subplots_adjust(left=0.25, right=0.75, top=0.75, bottom=0.15)
    plt.show()
    plt.show()


    # high_corr_list = ['tweet','nouns', 'adj', 'verbs', 'unique', 'stopwords', 'words','@USER','subtask_a']
    # train = pd.DataFrame(train, columns=high_corr_list)
    # train.to_csv('featured_train_a.csv')

    # 对task B构造特征
    # high_corr_list.append('subtask_b')
    # train['subtask_b'] = train_copy['subtask_b']
    # train = pd.DataFrame(train, columns=high_corr_list)
    # train.to_csv('featured_train_b.csv', index=False)

    # 对task C 的IND 类构造特征
    # high_corr_list = ['total_len','words', 'stopwords', 'PUNC', 'unique', 'nouns', 'adj', '@USER', 'subtask_c']
    # train['subtask_c'] = train_copy['subtask_c']
    # train = pd.DataFrame(train, columns=high_corr_list)
    # train.to_csv('featured_train_c.csv', index=False)